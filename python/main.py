import json
import socket
import sys


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.crashed = False  # flag to indicate if own car has crashed
        self.throttle_speed = 1.0
        self.turbo = False
        self.color = "Blue"

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, trackname, trackpword, trackcount):
        return self.msg("joinRace", {"botId": {
                                     "name": self.name,
                                     "key": self.key},
                    "trackName": trackname,
                    "password": trackpword,
                    "carCount": trackcount})

    def throttle(self):
        self.msg("throttle", self.throttle_speed)

    def swicth_lanes(self, direction):
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_custom(self, trackname, trackpword, trackcount):
        self.join_race(trackname, trackpword, trackcount)
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    # Game init: store track data, rival cars' data, and raceSession data
    def on_game_init(self, data):
        self.track_data = data["race"]["track"]
        self.store_track_pieces();
        self.cars_data = data["race"]["cars"] # TODO: monitor other cars
        self.race_data = data["race"]["raceSession"] # TODO: get race laps etc.
        print("Game init ", self.track_data["name"])
        print("Track pieces ", json.dumps(self.track_data["pieces"]))
        self.ping()

    # Store info about track pieces: straight or angle, "brake" piece
    def store_track_pieces(self):
        self.track_pieces = self.track_data["pieces"]
        self.track_pieces_len = len(self.track_pieces)
        for i in range(0, self.track_pieces_len-1):
            if "angle" in self.track_pieces[i+1]:
                self.track_pieces[i]["brake"] = True
                #print("Brake piece ", i)

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.decide_throttle(data)

    # calculate throttle based on car position data
    def decide_throttle(self, data):
        ourcar = self.get_our_car_info(data)
        # get the piece which we are on
        track_piece = self.track_pieces[ourcar["piecePosition"]["pieceIndex"]]
        if "length" in track_piece:
            print("Straigth piece")
            # push the brakes initially to the bottom, then release a little
            if "brake" in track_piece:
                if ourcar["piecePosition"]["inPieceDistance"] < 20:
                    self.throttle_speed = 0.0
                else:
                    self.throttle_speed = 0.1
                print("Angle ahead, braking, speed ", self.throttle_speed)
            else:
                self.throttle_speed = 1.0
            self.throttle()
        else:
            print("Angle piece")
            if self.throttle_speed == 1.0:
                self.throttle_speed = 0.1
            elif self.throttle_speed < 0.6:
                self.throttle_speed = self.throttle_speed + 0.1
            self.throttle()

    def on_crash(self, data):
        print("Someone crashed")
        if data["name"] == self.name: # check if we crashed
            self.crashed = True
            print("It was us!")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print(data)
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_spawn(self, data):
        print("Someone spawned")
        if data["name"] == self.name: # check if we spawned
            self.crashed = False
            print("It was us!")
        self.ping()

    def on_your_car(self, data):
        self.color = data["color"]
        print("We are " + self.color)
        self.ping()

    def on_finish(self, data):
        print(data)
        self.ping()

    def on_turbo_available(self, data):
        self.turbo = True
        self.ping()

    def get_our_car_info(self, data):
        for item in data:
            if item["id"]["name"] == self.name:
                return item
            else:
                print("Our car not found!")

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'yourCar': self.on_your_car,
            'finish': self.on_finish,
            'turboAvailable': self.on_turbo_available
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            #if msg_type != "carPositions":
            #    print(line) # test print messages
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) == 8:
        host, port, name, key, trackname, trackpword, trackcount = sys.argv[1:8]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}, trackname={4}, trackpword={5}, trackcount={6}")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run_custom(trackname, trackpword, trackcount)
    elif len(sys.argv) == 5:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
    else:
        print("Usage: './run host port botname botkey' OR './run host port botname botkey trackname trackpword trackcount'")
